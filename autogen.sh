#!/bin/sh

set -e
set -x

if ! autoreconf -i; then
    aclocal
    autoheader
    autoconf
    automake --foreign --add-missing --force-missing --copy
fi
